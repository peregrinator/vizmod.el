#+TITLE: vizmod

A nearly colourless theme based on something off of Reddit (don't have the link
on hand). Heavily inspired by [[htpps://reddit.com/user/n4rwoo][u/n4rw00's]] [[https://reddit.com/r/unixporn/comments/fzrglg/dwm_fell_off_the_tmux_bandwagon/][Reddit post]] which features a similar
theme for `vis-editor` (reminder to self: TRY `VIS` TODAY). This uses my fork of
the [[https://github.com/peregrinat0r/colourless-themes.el][colourless-themes]] macro.

** Screenshot(s)

/work in progress/

** Installation and usage

/work in progress/

** Terminal colours (WIP)

Also included is a terminal colour theme with nearly identical colours and
perhaps a colour or two more.
